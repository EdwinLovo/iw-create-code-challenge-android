# IW Pokédex

IW Pokédex es una aplicación nativa para el sistema operativo Android desarrollada en Koltin. Su objetivo es mostrar una pokédex completa de manera amigable para el usuario final. Permite ver el detalle de cada pokemon, guardar favoritos e incluso compartir algunos datos de cada pokemon.

## Descarga e instalación
Para descargar el archivo .apk, haz click en el siguiente enlace: [Descargar APK](https://drive.google.com/file/d/1G9pNuX9d10_SEW6OMX6CzF50_qNTU0Qf/view?usp=sharing)

### Pasos de instalación
- Descargar el archivo .apk.
- Verificar que el dispositivo permita instalar aplicaciones de orígenes desconocidos. Para habilitarlo, ir a configuración del dispositivo, en el apartado de Seguridad buscar "Aplicaciones de orígenes desconocidos" o similar; activar esta opción.
- Abrir el archivo .apk y darle instalar.
- Aceptar los permisos y darle siguiente hasta finalizar instalación.

## Tema oscuro
Si el dispositivo posee Android 10 y la opción de cambiar manualmente el tema de este, la aplicación es capaz de adaptarse al tema actual (En algunos dispositivos con Android puro puede que no se adapte).

<img src="images/homedark.jpg"  width="20%" height="25%"> <img src="images/detaildark.jpg"  width="20%" height="25%"> <img src="images/favoritesdark.jpg"  width="20%" height="25%">

## Tema claro

<img src="images/homelight.jpg"  width="20%" height="25%"> <img src="images/detailslight.jpg"  width="20%" height="25%"> <img src="images/favoriteslight.jpg"  width="20%" height="25%">


## Arquitecturas y patrones de diseño

Para esta aplicación, se hizo de uso la arquitectura de Paging Library la cual se integra con el patrón de diseño MVVM; para realizar paginación en las peticiones hechas a la PokéAPI y así realizar la carga de la información de manera más fluida y siguiendo las recomendaciones de Google.

<img src="https://miro.medium.com/max/875/1*djtgXO6T2Fm43sqn97gU-Q.png" width="50%" height="50%"> Paging Library Architecture (Google CodeLab)

De igual manera, se añadión Room Persistence Library a esta arquitectura, concretamente a la capa de datos. Esto se hizo para poder almacenar los favoritos en la memoria local del dispositivo y así poder verlos incluso cuando no se tenga conexión a internet.

<img src="https://developer.android.com/images/training/data-storage/room_architecture.png" width="30%" height="30%"> Room Architecture Diagram (Google developers)


## Componentes y librerías utilizadas en el desarrollo

- Model View ViewModel (MVVM)
- Paging Library
- Room Persistence Library
- Navigation Editor
- RxJava
- LiveData
- Coroutines
- Palette
- Material Design: [sitio web](https://material.io/design)
- Glide: [repositorio](https://github.com/bumptech/glide)
- Retrofit: [sitio web](https://square.github.io/retrofit/)

## Uso de Palette 
Palette Library es una librería de Google para Android que ayuda a detectar los colores principales o predominantes en una imagén. Por lo que se integró junto con Glide para así poder detectar el color predominante de cada pokemon y asignar dicho color a la CardView que lo contiene. 

## Recursos de terceros

- Icono de la aplicación, _**"Icon made by Pixel perfect from www.flaticon.com"**_
<img src="https://www.flaticon.com/svg/static/icons/svg/1033/1033032.svg" width="8%" height="8%">


- Fuente de texto: _**Monserrat, extraída de [Google Fonts](https://fonts.google.com)**_
