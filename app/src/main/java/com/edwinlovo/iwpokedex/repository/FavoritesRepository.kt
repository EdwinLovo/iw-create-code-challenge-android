package com.edwinlovo.iwpokedex.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.edwinlovo.iwpokedex.data.room.dao.FavoritePokemonDAO
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon

class FavoritesRepository (
    private val favoritePokemonDAO: FavoritePokemonDAO,
    private val pokemonId: Int
) {

    val allFavorites: LiveData<List<FavoritePokemon>> = favoritePokemonDAO.getAllFavoritePokemon()
    val isFavorite: LiveData<Boolean> = favoritePokemonDAO.isFavorite(pokemonId)

    @WorkerThread
    suspend fun insertFavorite(pokemon: FavoritePokemon) = favoritePokemonDAO.insert(pokemon)

    @WorkerThread
    fun deleteFavorite(pokemon: FavoritePokemon) = favoritePokemonDAO.deleteFavoritePokemon(pokemon)

    /*fun checkIfIsFavorite(id: Int): Boolean {
        return favoritePokemonDAO.isFavorite(id)
    }

    fun checkIsEmpty(): Boolean = favoritePokemonDAO.favoritesIsEmpty()*/

}