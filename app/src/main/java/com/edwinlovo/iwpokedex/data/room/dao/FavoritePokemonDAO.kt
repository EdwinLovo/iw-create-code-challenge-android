package com.edwinlovo.iwpokedex.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon

@Dao
interface FavoritePokemonDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(pokemon: FavoritePokemon)

    @Query("SELECT * FROM favoritepokemon")
    fun getAllFavoritePokemon(): LiveData<List<FavoritePokemon>>

    @Delete
    fun deleteFavoritePokemon(pokemon: FavoritePokemon)

    @Query("SELECT EXISTS(SELECT * FROM favoritepokemon WHERE id = :id)")
    fun isFavorite(id : Int) : LiveData<Boolean>

    /*@Query("SELECT EXISTS(SELECT * FROM favoritepokemon)")
    fun favoritesIsEmpty(): Boolean*/

}