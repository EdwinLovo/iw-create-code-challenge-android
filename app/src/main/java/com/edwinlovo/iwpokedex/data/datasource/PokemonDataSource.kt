package com.edwinlovo.iwpokedex.data.datasource

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.edwinlovo.iwpokedex.data.api.ApiService
import com.edwinlovo.iwpokedex.data.model.pokemon_detail.Pokemon
import com.edwinlovo.iwpokedex.data.api.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class PokemonDataSource (
    private val apiService: ApiService,
    private val compositeDisposable: CompositeDisposable
) {

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
    get() = _networkState

    private val _pokemonDetailResponse = MutableLiveData<Pokemon>()
    val pokemonDetailResponse: LiveData<Pokemon>
    get() = _pokemonDetailResponse

    fun fetchPokemonDetails(pokemonId: Int) {
        _networkState.postValue(NetworkState.LOADING)

        try {
            compositeDisposable.add(
                apiService.getPokemonDetail(pokemonId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _pokemonDetailResponse.postValue(it)
                            _networkState.postValue(NetworkState.DONE)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("PokemonDataSource: ", it.message)
                        }
                    )
            )
        } catch (e:Exception){
            Log.e("PokemonDataSource: ", e.message)
        }
    }

}