package com.edwinlovo.iwpokedex.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.edwinlovo.iwpokedex.data.room.dao.FavoritePokemonDAO
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon

@Database(entities = [FavoritePokemon::class], version = 3, exportSchema = false)
abstract class RoomDB: RoomDatabase() {

    abstract fun favoritePokemonDao(): FavoritePokemonDAO

    companion object {
        @Volatile
        private var INSTANCE: RoomDB? = null

        fun getInstance(
            context: Context
        ): RoomDB {
            if (INSTANCE != null) {
                return  INSTANCE!!
            } else {
                synchronized(this) {
                    INSTANCE = Room
                        .databaseBuilder(context, RoomDB::class.java, "PokemonDB")
                        .fallbackToDestructiveMigration()
                        .build()
                    return  INSTANCE!!
                }
            }
        }
    }

}