package com.edwinlovo.iwpokedex.data.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favoritepokemon")
data class FavoritePokemon (

    @PrimaryKey
    val id: Int,
    val url: String,
    val name: String,
    val height: Int,
    val weight: Int,
    val exp: Int

)