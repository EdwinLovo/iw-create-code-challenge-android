package com.edwinlovo.iwpokedex.ui.favorites

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon
import com.edwinlovo.iwpokedex.ui.adapter.FavoritesAdapter
import com.edwinlovo.iwpokedex.ui.viewholders.FavoritesViewHolder
import com.edwinlovo.iwpokedex.viewmodel.FavoritesViewModel
import kotlinx.android.synthetic.main.favorite_pokemon_item.view.*
import kotlinx.android.synthetic.main.fragment_favorites.view.*

class FavoritesFragment : Fragment() {

    lateinit var viewModel: FavoritesViewModel
    var appVM: Application? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        activity?.title = "Mis favoritos"
        appVM = activity?.application
        bind(view)
        return view
    }

    private fun bind(view: View) {
        viewModel = getViewModel(0)
        val adapter = object : FavoritesAdapter(view.context) {
            override fun setCheckListener(
                favorite: FavoritePokemon,
                holder: FavoritesViewHolder
            ) {
                holder.itemView.favorite_icon.isChecked = true
                holder.itemView.favorite_icon.setOnCheckedChangeListener { checkBox, isChecked ->
                    if (!isChecked) {
                        viewModel.deleteFavorite(favorite)
                    }
                }
            }
        }
        val recyclerView = view.rv_favorite

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(view.context)

        viewModel.allFavorites.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.isEmpty()) {
                    view.ll_no_favorites.visibility = View.VISIBLE
                } else {
                    view.ll_no_favorites.visibility = View.GONE
                }
                adapter.setFavorites(it)
            }
        })


    }

    private fun getViewModel(id: Int): FavoritesViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return appVM?.let {
                    FavoritesViewModel(
                        it,
                        id
                    )
                } as T
            }
        })[FavoritesViewModel::class.java]
    }

}