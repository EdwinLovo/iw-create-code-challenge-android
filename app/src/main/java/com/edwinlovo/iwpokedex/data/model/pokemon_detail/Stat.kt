package com.edwinlovo.iwpokedex.data.model.pokemon_detail


data class Stat(
    val stat: StatX
)