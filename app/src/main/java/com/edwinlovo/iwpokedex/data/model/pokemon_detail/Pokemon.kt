package com.edwinlovo.iwpokedex.data.model.pokemon_detail


import com.google.gson.annotations.SerializedName

data class Pokemon(
    val abilities: List<Ability>,
    @SerializedName("base_experience")
    val baseExperience: Int,
    val forms: List<Form>,
    val height: Int,
    @SerializedName("held_items")
    val heldItems: List<HeldItem>,
    val id: Int,
    val moves: List<Move>,
    val name: String,
    val stats: List<Stat>,
    val weight: Int
)