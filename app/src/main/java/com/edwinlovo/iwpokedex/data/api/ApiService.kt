package com.edwinlovo.iwpokedex.data.api

import com.edwinlovo.iwpokedex.data.model.pokedex_list.Pokedex
import com.edwinlovo.iwpokedex.data.model.pokemon_detail.Pokemon
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    // https://pokeapi.co/api/v2/pokemon?limit=20&offset=0
    // https://pokeapi.co/api/v2/pokemon/132

    @GET("pokemon")
    fun getAllPokemon(@Query("offset") offset: Int): Single<Pokedex>

    @GET("pokemon/{pokemon_id}")
    fun getPokemonDetail(@Path("pokemon_id") id: Int): Single<Pokemon>

    companion object {
        fun getService(): ApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl("")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }

}