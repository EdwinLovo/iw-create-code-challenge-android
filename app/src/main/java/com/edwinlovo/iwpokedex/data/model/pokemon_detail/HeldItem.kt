package com.edwinlovo.iwpokedex.data.model.pokemon_detail


import com.google.gson.annotations.SerializedName

data class HeldItem(
    val item: Item
)