package com.edwinlovo.iwpokedex.data.datasource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.edwinlovo.iwpokedex.data.api.ApiService
import com.edwinlovo.iwpokedex.data.api.LIMIT
import com.edwinlovo.iwpokedex.data.api.OFFSET
import com.edwinlovo.iwpokedex.data.model.pokedex_list.Result
import com.edwinlovo.iwpokedex.data.api.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PokedexDataSource (
    private val apiService: ApiService,
    private val compositeDisposable: CompositeDisposable
): PageKeyedDataSource<Int, Result>() {

    private var offset = OFFSET
    private var limit = LIMIT
    val networkState: MutableLiveData<NetworkState> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Result>
    ) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getAllPokemon(offset)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        callback.onResult(it.results, null, offset+20)
                        networkState.postValue(NetworkState.DONE)
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("PokedexDataSource: ", it.message)
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getAllPokemon(params.key)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        if ((it.count-params.key) > 0) {
                            callback.onResult(it.results, params.key+20)
                            networkState.postValue(NetworkState.DONE)
                        } else {
                            networkState.postValue(NetworkState.ENDOFLIST)
                        }
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("PokedexDataSource: ", it.message)
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        TODO("Not yet implemented")
    }


}