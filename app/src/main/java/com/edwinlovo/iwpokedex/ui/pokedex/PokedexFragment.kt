package com.edwinlovo.iwpokedex.ui.pokedex

import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.api.ApiClient
import com.edwinlovo.iwpokedex.data.api.ApiService
import com.edwinlovo.iwpokedex.repository.PokedexRepository
import com.edwinlovo.iwpokedex.ui.adapter.PokedexAdapter
import com.edwinlovo.iwpokedex.data.api.NetworkState
import com.edwinlovo.iwpokedex.viewmodel.PokedexViewModel
import kotlinx.android.synthetic.main.fragment_pokedex.*
import kotlinx.android.synthetic.main.fragment_pokedex.view.*

class PokedexFragment : Fragment() {

    private lateinit var viewModel: PokedexViewModel
    lateinit var pokedexRepository: PokedexRepository
    lateinit var viewFragment: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_pokedex, container, false)
        activity?.title = "IW Pokédex"
        val apiService: ApiService = ApiClient.getService()
        pokedexRepository = PokedexRepository(apiService)
        viewModel = getViewModel()
        val pokedexAdapter =
            PokedexAdapter(viewFragment.context)

        var gridLayoutManager = GridLayoutManager(viewFragment.context, 2)
        if (isLanscape()) {
            gridLayoutManager = GridLayoutManager(viewFragment.context, 4);
        }

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val viewType = pokedexAdapter.getItemViewType(position)
                if (viewType == pokedexAdapter.POKEMON_VIEW_TYPE) return 1
                else return 2
            }
        }

        viewFragment.rv_pokedex_fragment.layoutManager = gridLayoutManager
        viewFragment.rv_pokedex_fragment.setHasFixedSize(true)
        viewFragment.rv_pokedex_fragment.adapter = pokedexAdapter

        viewModel.pokedexPagedList.observe(viewLifecycleOwner, Observer {
            pokedexAdapter.submitList(it)
        })

        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            progress_bar_pokedex.visibility = if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error_pokedex.visibility = if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE

            if (!viewModel.listIsEmpty()) {
                pokedexAdapter.setNetworkState(it)
            }
        })

        return viewFragment
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.pokedex_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        R.id.action_favorites -> {
            Navigation.findNavController(viewFragment).navigate(R.id.action_pokedexFragment_to_favoritesFragment)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun getViewModel(): PokedexViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return PokedexViewModel(
                    pokedexRepository
                ) as T
            }
        })[PokedexViewModel::class.java]
    }

    private fun isLanscape():Boolean=resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

}