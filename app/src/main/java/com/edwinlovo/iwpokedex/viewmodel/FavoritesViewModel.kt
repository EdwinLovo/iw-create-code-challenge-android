package com.edwinlovo.iwpokedex.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.edwinlovo.iwpokedex.data.room.RoomDB
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon
import com.edwinlovo.iwpokedex.repository.FavoritesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoritesViewModel (
    private val app: Application,
    private val pokemonId: Int
): AndroidViewModel(app) {

    private val favoritesRepository: FavoritesRepository
    val allFavorites: LiveData<List<FavoritePokemon>>
    val isFavorite: LiveData<Boolean>

    init {
        val favoritePokemonDAO = RoomDB.getInstance(app).favoritePokemonDao()
        favoritesRepository = FavoritesRepository(favoritePokemonDAO, pokemonId)
        allFavorites = favoritesRepository.allFavorites
        isFavorite = favoritesRepository.isFavorite
    }

    fun insertFavorite(pokemon: FavoritePokemon) = viewModelScope.launch(Dispatchers.IO) {
        favoritesRepository.insertFavorite(pokemon)
    }

    fun deleteFavorite(pokemon: FavoritePokemon) = viewModelScope.launch(Dispatchers.IO) {
        favoritesRepository.deleteFavorite(pokemon)
    }

    /*fun checkIsEmpty():Boolean {
        return  favoritesRepository.checkIsEmpty()
    }

    fun checkIsFavorite(id: Int):Boolean {
        return  favoritesRepository.checkIfIsFavorite(id)
    }*/

}