package com.edwinlovo.iwpokedex.data.model.pokemon_detail


import com.google.gson.annotations.SerializedName

data class MoveX(
    val name: String
)