package com.edwinlovo.iwpokedex.data.model.pokemon_detail


data class Ability(
    val ability: AbilityX
)