package com.edwinlovo.iwpokedex.ui.pokemon_detail

import android.app.Application
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.api.*
import com.edwinlovo.iwpokedex.data.model.pokemon_detail.Pokemon
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon
import com.edwinlovo.iwpokedex.repository.PokedexRepository
import com.edwinlovo.iwpokedex.viewmodel.FavoritesViewModel
import com.edwinlovo.iwpokedex.viewmodel.PokemonViewModel
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.fragment_pokemon.*
import kotlinx.android.synthetic.main.fragment_pokemon.view.*

class PokemonFragment : Fragment() {

    private lateinit var viewModel: PokemonViewModel
    private lateinit var favoritesViewModel: FavoritesViewModel
    private lateinit var pokedexRepository: PokedexRepository

    private var pokemonId: Int = 0
    private var pokemonName: String = ""
    private var pokemonColor: Int = 0
    private var appVM:Application? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pokemon, container, false)
        setHasOptionsMenu(true)

        appVM = activity?.application
        pokemonId = arguments?.getInt("pokemonId", 0) ?: 0
        pokemonName = arguments?.getString("pokemonName", "IW Pokédex").toString()
        val title = "#$pokemonId $pokemonName"
        activity?.title = title

        val apiService: ApiService = ApiClient.getService()
        pokedexRepository = PokedexRepository(apiService)
        favoritesViewModel = getFavoriteViewModel(pokemonId)
        viewModel = getViewModel(pokemonId)

        viewModel.pokemonDetails.observe(viewLifecycleOwner, Observer {
            bindUI(it, view)
        })

        favoritesViewModel.isFavorite.observe(viewLifecycleOwner, Observer {
            view.pokemon_favorite_icon.isChecked = it
        })

        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            progress_bar_pokemon.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error_pokemon.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE
        })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.pokemon_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        R.id.action_share -> {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                val pokemonUrl = "IW Pokédex Share \nNombre: "+pokemonName+"\nURL: "+BASE_URL+"pokemon/"+pokemonId
                putExtra(Intent.EXTRA_TEXT, pokemonUrl)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    fun bindUI(pokemon: Pokemon, view: View) {
        val pokemonSprite = POKEMON_SPRITE_BASE_URL +pokemon.id+".png"
        txt_base_exp.text = getString(R.string.pokemon_exp, pokemon.baseExperience)
        txt_height.text = getString(R.string.pokemon_height, pokemon.height)
        txt_weight.text = getString(R.string.pokemon_weight, pokemon.weight)

        view.pokemon_favorite_icon.setOnCheckedChangeListener { checkBox, isChecked ->
            val favorite = FavoritePokemon(
                pokemon.id, pokemonSprite, pokemon.name, pokemon.height, pokemon.weight, pokemon.baseExperience)
            if (isChecked) {
                favoritesViewModel.insertFavorite(favorite)
            } else {
                favoritesViewModel.deleteFavorite(favorite)
            }
        }

        val cgStats = cg_stats
        for (s in pokemon.stats) {
            val chip = Chip(cgStats.context)
            chip.text = s.stat.name
            chip.isCheckable = false
            chip.isClickable = false
            cgStats.addView(chip)
        }

        val cgMovs = cg_movs
        for (m in pokemon.moves){
            val chip = Chip(cgMovs.context)
            chip.text = m.move.name
            chip.isCheckable = false
            chip.isClickable = false
            cgMovs.addView(chip)
        }

        val cgSkills = cg_skills
        for (a in pokemon.abilities){
            val chip = Chip(cgSkills.context)
            chip.text = a.ability.name
            chip.isCheckable = false
            chip.isClickable = false
            cgSkills.addView(chip)
        }

        if (pokemon.heldItems.isNotEmpty()){
            ll_held_items.visibility = View.VISIBLE
            val cgItems = cg_held_items
            for (i in pokemon.heldItems){
                val chip = Chip(cgItems.context)
                chip.text = i.item.name
                chip.isCheckable = false
                chip.isClickable = false
                cgItems.addView(chip)
            }
        }

        Glide.with(this)
            .asBitmap()
            .load(pokemonSprite)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.mipmap.ic_app_icon_large)
            .error(R.mipmap.ic_app_icon_large)
            .into(object : CustomTarget<Bitmap>(){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    createPaletteAsync(resource, view)
                    iv_pokemon.setImageBitmap(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })
    }

    // Function to get the most dominant color of the pokemon sprite
    // and set it to the cardview
    private fun createPaletteAsync(
        bitmap: Bitmap,
        view: View
    ) {
        Palette.from(bitmap).generate {
            val color = it?.dominantSwatch?.rgb
            pokemonColor = color ?: ContextCompat.getColor(view.context,
                R.color.colorPrimary
            )
            Log.e("COLOR: ", color.toString())
            with(ll_pokemon_sprite) {
                setCardBackgroundColor(color ?: androidx.core.content.ContextCompat.getColor(view.context,
                    R.color.cardviewBG
                ))
            }
        }
    }

    private fun getViewModel(id: Int): PokemonViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return PokemonViewModel(
                    pokedexRepository,
                    id
                ) as T
            }
        })[PokemonViewModel::class.java]
    }

    private fun getFavoriteViewModel(id: Int): FavoritesViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return appVM?.let {
                    FavoritesViewModel(
                        it,
                        id
                    )
                } as T
            }
        })[FavoritesViewModel::class.java]
    }

}