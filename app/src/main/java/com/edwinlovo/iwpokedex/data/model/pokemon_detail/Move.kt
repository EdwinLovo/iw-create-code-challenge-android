package com.edwinlovo.iwpokedex.data.model.pokemon_detail


data class Move(
    val move: MoveX
)