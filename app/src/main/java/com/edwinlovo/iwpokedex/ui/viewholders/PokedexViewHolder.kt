package com.edwinlovo.iwpokedex.ui.viewholders

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.Navigation
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.api.POKEMON_SPRITE_BASE_URL
import com.edwinlovo.iwpokedex.data.model.pokedex_list.Result
import kotlinx.android.synthetic.main.pokedex_list_item.view.*

class PokedexViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bind(pokemon: Result?, context: Context) {
        val urlPartes = pokemon?.url!!.split("/".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
        val pokemon_id =  Integer.parseInt(urlPartes[urlPartes.size - 1])
        val pokemonPosterURL = "$POKEMON_SPRITE_BASE_URL$pokemon_id.png"

        itemView.cv_pokemon_id.text = """#$pokemon_id"""
        itemView.cv_pokemon_name.text = pokemon.name.capitalize()

        /*Glide.with(itemView.context)
            .load(pokemonPosterURL)
            .error(R.drawable.ic_not_found)
            .placeholder(R.drawable.ic_not_found)
            .into(itemView.cv_iv_pokemon)*/
        Log.e("COLOR: ", pokemon.name)
        Glide.with(context)
            .asBitmap()
            .load(pokemonPosterURL)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.mipmap.ic_app_icon_large)
            .error(R.mipmap.ic_app_icon_large)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    createPaletteAsync(resource)
                    itemView.cv_iv_pokemon.setImageBitmap(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
        


        Glide.with(context).clear(itemView.cv_iv_pokemon)

        itemView.setOnClickListener {
            val args = Bundle()
            args.putInt("pokemonId", pokemon_id)
            args.putString("pokemonName", pokemon.name.capitalize())
            Navigation.findNavController(itemView).navigate(R.id.action_pokedexFragment_to_pokemonFragment, args)
        }
    }

    // Function to get the most dominant color of the pokemon sprite
    // and set it to the cardview
    private fun createPaletteAsync(bitmap: Bitmap) {
        Palette.from(bitmap).generate {
            val color = it?.dominantSwatch?.rgb
            Log.e("COLOR: ", color.toString())
            with(itemView.card_view) {
                setCardBackgroundColor(color ?: androidx.core.content.ContextCompat.getColor(itemView.context, com.edwinlovo.iwpokedex.R.color.cardviewBG))
            }
        }
    }

}
