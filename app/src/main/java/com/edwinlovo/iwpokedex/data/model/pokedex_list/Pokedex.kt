package com.edwinlovo.iwpokedex.data.model.pokedex_list


data class Pokedex(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<Result>
)