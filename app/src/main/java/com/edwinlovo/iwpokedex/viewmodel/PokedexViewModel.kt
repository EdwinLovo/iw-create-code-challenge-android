package com.edwinlovo.iwpokedex.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.edwinlovo.iwpokedex.data.model.pokedex_list.Result
import com.edwinlovo.iwpokedex.repository.PokedexRepository
import com.edwinlovo.iwpokedex.data.api.NetworkState
import io.reactivex.disposables.CompositeDisposable

class PokedexViewModel (private val pokedexRepository: PokedexRepository): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val pokedexPagedList: LiveData<PagedList<Result>> by lazy {
        pokedexRepository.fetchLivePokedexPagedList(compositeDisposable)
    }

    val networkState: LiveData<NetworkState> by lazy {
        pokedexRepository.getNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return pokedexPagedList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}