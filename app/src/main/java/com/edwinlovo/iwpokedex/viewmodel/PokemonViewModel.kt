package com.edwinlovo.iwpokedex.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.edwinlovo.iwpokedex.data.model.pokemon_detail.Pokemon
import com.edwinlovo.iwpokedex.repository.PokedexRepository
import com.edwinlovo.iwpokedex.data.api.NetworkState
import io.reactivex.disposables.CompositeDisposable

class PokemonViewModel (
    private val pokedexRepository: PokedexRepository,
    pokemonId: Int
): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val pokemonDetails: LiveData<Pokemon> by lazy {
        pokedexRepository.fetchPokemonDetails(compositeDisposable, pokemonId)
    }

    val networkState: LiveData<NetworkState> by lazy {
        pokedexRepository.getPokemonDetailsNetworkState()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}