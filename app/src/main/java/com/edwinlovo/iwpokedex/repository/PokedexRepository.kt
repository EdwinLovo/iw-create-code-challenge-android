package com.edwinlovo.iwpokedex.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.edwinlovo.iwpokedex.data.api.ApiService
import com.edwinlovo.iwpokedex.data.api.LIMIT
import com.edwinlovo.iwpokedex.data.model.pokedex_list.Result
import com.edwinlovo.iwpokedex.data.model.pokemon_detail.Pokemon
import com.edwinlovo.iwpokedex.data.datasource.PokedexDataSource
import com.edwinlovo.iwpokedex.data.datasource.PokedexDataSourceFactory
import com.edwinlovo.iwpokedex.data.datasource.PokemonDataSource
import com.edwinlovo.iwpokedex.data.api.NetworkState
import io.reactivex.disposables.CompositeDisposable

class PokedexRepository (private val apiService: ApiService) {

    // Code for pokedex
    lateinit var pokedexPagedList: LiveData<PagedList<Result>>
    lateinit var pokedexDataSourceFactory: PokedexDataSourceFactory

    fun fetchLivePokedexPagedList (compositeDisposable: CompositeDisposable): LiveData<PagedList<Result>> {
        pokedexDataSourceFactory = PokedexDataSourceFactory(apiService, compositeDisposable)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(LIMIT)
            .build()

        pokedexPagedList = LivePagedListBuilder(pokedexDataSourceFactory, config).build()

        return pokedexPagedList
    }

    fun getNetworkState(): LiveData<NetworkState> {
        return Transformations.switchMap<PokedexDataSource, NetworkState>(
            pokedexDataSourceFactory.pokedexLiveDataSource, PokedexDataSource::networkState
        )
    }


    // code for each pokemon details
    lateinit var pokemonDataSource: PokemonDataSource

    fun fetchPokemonDetails(compositeDisposable: CompositeDisposable, pokemonId: Int): LiveData<Pokemon> {
        pokemonDataSource = PokemonDataSource(apiService, compositeDisposable)
        pokemonDataSource.fetchPokemonDetails(pokemonId)

        return pokemonDataSource.pokemonDetailResponse
    }

    fun getPokemonDetailsNetworkState(): LiveData<NetworkState> {
        return pokemonDataSource.networkState
    }

}