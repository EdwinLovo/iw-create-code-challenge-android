package com.edwinlovo.iwpokedex.data.api

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

class NetworkState(val status: Status, val msg: String) {

    companion object {
        val DONE: NetworkState
        val LOADING: NetworkState
        val ERROR: NetworkState
        val ENDOFLIST: NetworkState

        init {
            DONE =
                NetworkState(
                    Status.SUCCESS,
                    "Finalizado"
                )
            LOADING =
                NetworkState(
                    Status.RUNNING,
                    "Cargando..."
                )
            ERROR =
                NetworkState(
                    Status.FAILED,
                    "Ocurrió un error..."
                )
            ENDOFLIST =
                NetworkState(
                    Status.FAILED,
                    "Has alcanzado el final"
                )
        }
    }

}