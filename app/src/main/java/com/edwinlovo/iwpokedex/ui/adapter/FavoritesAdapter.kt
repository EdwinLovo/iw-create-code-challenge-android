package com.edwinlovo.iwpokedex.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon
import com.edwinlovo.iwpokedex.ui.viewholders.FavoritesViewHolder

abstract class FavoritesAdapter (private val context: Context)
    :RecyclerView.Adapter<FavoritesViewHolder>() {

    private var inflater = LayoutInflater.from(context)
    private var favorites = emptyList<FavoritePokemon>()

    abstract fun setCheckListener(favorite:FavoritePokemon, holder: FavoritesViewHolder)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val itemView = inflater.inflate(R.layout.favorite_pokemon_item,  parent, false)
        return FavoritesViewHolder(itemView)
    }

    override fun getItemCount() = favorites.size

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        holder.bind(favorites[position], context)
        setCheckListener(favorites[position], holder)
    }

    internal fun setFavorites(favorites: List<FavoritePokemon>) {
        this.favorites = favorites
        notifyDataSetChanged()
    }
}