package com.edwinlovo.iwpokedex.data.model.pokedex_list


import com.google.gson.annotations.SerializedName

data class Result(
    val name: String,
    val url: String

)