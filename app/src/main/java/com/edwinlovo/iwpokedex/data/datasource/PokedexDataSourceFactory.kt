package com.edwinlovo.iwpokedex.data.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.edwinlovo.iwpokedex.data.api.ApiService
import com.edwinlovo.iwpokedex.data.model.pokedex_list.Result
import io.reactivex.disposables.CompositeDisposable

class PokedexDataSourceFactory (
    private val apiService: ApiService,
    private val compositeDisposable: CompositeDisposable
): DataSource.Factory<Int, Result>() {

    val pokedexLiveDataSource = MutableLiveData<PokedexDataSource>()

    override fun create(): DataSource<Int, Result> {
        val pokedexDataSource = PokedexDataSource(apiService, compositeDisposable)
        pokedexLiveDataSource.postValue(pokedexDataSource)

        return  pokedexDataSource
    }


}