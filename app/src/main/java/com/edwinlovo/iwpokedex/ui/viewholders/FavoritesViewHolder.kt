package com.edwinlovo.iwpokedex.ui.viewholders

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.Navigation
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.edwinlovo.iwpokedex.R
import com.edwinlovo.iwpokedex.data.room.entities.FavoritePokemon
import kotlinx.android.synthetic.main.favorite_pokemon_item.view.*
import kotlinx.android.synthetic.main.fragment_pokemon.view.*
import kotlinx.android.synthetic.main.pokedex_list_item.view.*

class FavoritesViewHolder (view: View): RecyclerView.ViewHolder(view) {

    fun bind(pokemon: FavoritePokemon, context: Context) {
        itemView.txt_favorite_exp.text = context.getString(R.string.favorite_exp, pokemon.exp)
        itemView.txt_favorite_height.text = context.getString(R.string.favorite_height, pokemon.height)
        itemView.txt_favorite_weight.text = context.getString(R.string.favorite_weight, pokemon.weight)
        itemView.txt_favorite_name.text = context.getString(R.string.favorite_id_name, pokemon.id, pokemon.name)

        Glide.with(context)
            .asBitmap()
            .load(pokemon.url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.mipmap.ic_app_icon_large)
            .error(R.mipmap.ic_app_icon_large)
            .into(object : CustomTarget<Bitmap>(){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    createPaletteAsync(resource)
                    itemView.iv_favorite_sprite.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })

        itemView.setOnClickListener {
            val args = Bundle()
            args.putInt("pokemonId", pokemon.id)
            args.putString("pokemonName", pokemon.name.capitalize())
            Navigation.findNavController(itemView).navigate(R.id.action_favoritesFragment_to_pokemonFragment, args)
        }

    }

    // Function to get the most dominant color of the pokemon sprite
    // and set it to the cardview
    private fun createPaletteAsync(bitmap: Bitmap) {
        Palette.from(bitmap).generate {
            val color = it?.dominantSwatch?.rgb
            Log.e("COLOR: ", color.toString())
            with(itemView.iv_favorite_sprite) {
                setBackgroundColor(color ?: androidx.core.content.ContextCompat.getColor(itemView.context, com.edwinlovo.iwpokedex.R.color.cardviewBG))
            }
        }
    }

}